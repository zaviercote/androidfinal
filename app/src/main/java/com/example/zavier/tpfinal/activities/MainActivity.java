package com.example.zavier.tpfinal.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.zavier.tpfinal.R;
import com.example.zavier.tpfinal.beans.League;
import com.example.zavier.tpfinal.data.HttpRequestTask;

public class MainActivity extends AppCompatActivity {

    public static League currentLeague = null;
    static final int LOGIN_REQUEST_CODE =1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        currentLeague = null;
       Intent loginIntent = new Intent(this,LoginActivity.class);
       startActivityForResult(loginIntent,LOGIN_REQUEST_CODE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        new HttpRequestTask().execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST_CODE){
            if(resultCode == RESULT_OK){
               final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Welcome!");
                alertDialog.setMessage("Welcome");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.dismiss();
                    }
                });
            }
        }
        Intent intent = new Intent(this,TeamsViewActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu,menu);
        return true;
    }
}
