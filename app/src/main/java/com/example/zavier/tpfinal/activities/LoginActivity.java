package com.example.zavier.tpfinal.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.zavier.tpfinal.R;
import com.example.zavier.tpfinal.fragments.LoginDialogFragment;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginDialogFragment loginDialog = new LoginDialogFragment();
        loginDialog.show(getSupportFragmentManager(),"login");
        setResult(RESULT_OK);
    }

    @Override
    public void onBackPressed() {
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu,menu);
        return true;
    }
}
