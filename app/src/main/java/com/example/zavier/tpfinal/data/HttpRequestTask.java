package com.example.zavier.tpfinal.data;

import android.os.AsyncTask;
import android.util.Log;

import com.example.zavier.tpfinal.activities.MainActivity;
import com.example.zavier.tpfinal.beans.League;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class HttpRequestTask extends AsyncTask<Void,Void,League> {
    @Override
    protected League doInBackground(Void... voids) {
       try {
           RestTemplate restTemplate = new RestTemplate();
           restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
           //changer url a adress ip local!!!!!
           String url = "http://192.168.11.121:8080/QJFL";
           League league = restTemplate.getForObject(url, League.class);
           return league;
       }catch (Exception e){
           Log.e("HTTP_REQUEST",e.getMessage());
       }
        return null;
    }

    @Override
    protected void onPostExecute(League league) {
        if (league == null){
            League backupLeague = League.genQJFL();
            backupLeague.genTeamScheduels();
            MainActivity.currentLeague = league;

        }
        else {
            MainActivity.currentLeague = league;
            MainActivity.currentLeague.genTeamScheduels();
        }
    }
}
