package com.example.zavier.tpfinal.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.zavier.tpfinal.R;
import com.example.zavier.tpfinal.beans.Game;
import com.example.zavier.tpfinal.beans.Team;
import com.example.zavier.tpfinal.fragments.GameFragment;

import java.util.ArrayList;
import java.util.List;

public class GameBrowserActivity extends AppCompatActivity{

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerAdapter pagerAdapter;

    private Team team;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.games_pager);
        Bundle bundle = getIntent().getExtras();
        int teamId = bundle.getInt("teamId");

        team = MainActivity.currentLeague.getTeams().get(teamId);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        List<GameFragment> fragments = new ArrayList<>();

        for(Game selectedGame:team.getScheduel().getGames()){
            Team homeTeam = selectedGame.getHomeTeam();
            Team awayTeam = selectedGame.getAwayTeam();
            fragments.add(GameFragment.newInstance(awayTeam.getName(),homeTeam.getName(),selectedGame.getAwayTeamScore(),selectedGame.getHomeTeamScore(),selectedGame.getLocation(),selectedGame.isFinished()));
        }

        pagerAdapter = new GameBrowserAdapter(getSupportFragmentManager(),fragments);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private class GameBrowserAdapter extends FragmentStatePagerAdapter{

        List<GameFragment> fragments;

        public GameBrowserAdapter(FragmentManager fm, List<GameFragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
           return fragments.get(position);
        }

        @Override
        public int getCount() {
            return team.getScheduel().getGames().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "G"+(position+1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu,menu);
        return true;
    }
}
