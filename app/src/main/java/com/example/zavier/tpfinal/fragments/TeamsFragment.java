package com.example.zavier.tpfinal.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.zavier.tpfinal.R;
import com.example.zavier.tpfinal.activities.GameBrowserActivity;
import com.example.zavier.tpfinal.activities.MainActivity;

public class TeamsFragment extends ListFragment {

    String teams[] = MainActivity.currentLeague.getTeamNames();

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.team_dialog_title))
                .setMessage(getResources().getString(R.string.team_dialog_message) +" "+ teams[position])
                .setPositiveButton(getResources().getString(R.string.team_dialog_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent gameBrowserIntent = new Intent(getContext(), GameBrowserActivity.class);
                        gameBrowserIntent.putExtra("teamId",position);
                        startActivity(gameBrowserIntent);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.teams_fragment,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, teams);
        setListAdapter(arrayAdapter);
    }
}
