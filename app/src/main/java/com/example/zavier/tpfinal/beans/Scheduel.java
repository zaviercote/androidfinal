package com.example.zavier.tpfinal.beans;

import java.util.ArrayList;
import java.util.List;

public class Scheduel {

    private int id;
    private List<Game> games;

    public Scheduel(){}
    public Scheduel(int id) {
        this.id = id;
        this.games = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public void addGame(Game game){
        this.getGames().add(game);
    }

    @Override
    public String toString() {
        return "Scheduel{" +
                "id=" + id +
                ", games=" + games.size() +
                '}';
    }
}
