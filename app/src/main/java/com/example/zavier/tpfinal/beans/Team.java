package com.example.zavier.tpfinal.beans;

public class Team {

    private int id;
    private String name;
    private Scheduel scheduel;
    public Team(){}


    public Team(int id, String name) {
        this.id = id;
        this.name = name;
        this.scheduel = new Scheduel(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Scheduel getScheduel() {
        return scheduel;
    }

    public void setScheduel(Scheduel scheduel) {
        this.scheduel = scheduel;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", scheduel=" + scheduel +
                '}';
    }

    public int wins(){
        int wins = 0;
        for (Game game:this.scheduel.getGames()) {
            if(game.getAwayTeam().getName() == this.getName()){
                if (game.getAwayTeamScore() > game.getHomeTeamScore()){
                    wins++;
                }
            }
            else{
                if(game.getHomeTeamScore() > game.getAwayTeamScore()){
                    wins++;
                }
            }
        }
        return wins;
    }
}
