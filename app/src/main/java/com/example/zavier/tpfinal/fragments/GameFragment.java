package com.example.zavier.tpfinal.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zavier.tpfinal.R;

public class GameFragment extends Fragment {

    ImageView awayImage;
    ImageView homeImage;

    TextView awayName;
    TextView homeName;

    TextView awayScore;
    TextView homeScore;

    TextView location;
    TextView finished;

    public String awayTeamName;
    String homeTeamName;

    int awayTeamScore;
    int homeTeamScore;

    String gameLocation;
    boolean gameFinished;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return initView(inflater, container);
    }

    @NonNull
    private View initView(LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(R.layout.game_fragment,container,false);

        awayImage = (ImageView) view.findViewById(R.id.awayLogo);
        homeImage = (ImageView) view.findViewById(R.id.homeLogo);

        awayScore = (TextView) view.findViewById(R.id.awayTeamScore);
        homeScore = (TextView) view.findViewById(R.id.homeTeamScore);

        awayName = (TextView) view.findViewById(R.id.awayTeamName);
        homeName = (TextView) view.findViewById(R.id.homeTeamName);

        location = (TextView) view.findViewById(R.id.location);
        finished = (TextView) view.findViewById(R.id.finished);

        awayImage.setImageResource(getTeamLogo(awayTeamName));
        homeImage.setImageResource(getTeamLogo(homeTeamName));

        awayName.setText(awayTeamName);
        homeName.setText(homeTeamName);

        awayScore.setText(String.valueOf(awayTeamScore));
        homeScore.setText(String.valueOf(homeTeamScore));

        location.setText(gameLocation);
        if (gameFinished){
            finished.setText(getResources().getString(R.string.game_final_score));
        }
        else {
            finished.setText(getResources().getString(R.string.game_not_final));
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static GameFragment newInstance(String awayTeamName,String homeTeamName,int awayTeamScore,int homeTeamScore,String gameLocation,boolean gameFinished) {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putString("awayTeamName", awayTeamName);
        args.putString("homeTeamName", homeTeamName);

        args.putInt("awayTeamScore", awayTeamScore);
        args.putInt("homeTeamScore", homeTeamScore);

        args.putString("gameLocation", gameLocation);
        args.putBoolean("gameFinished",gameFinished);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            awayTeamName = getArguments().getString("awayTeamName");
            homeTeamName= getArguments().getString("homeTeamName");

            awayTeamScore= getArguments().getInt("awayTeamScore");
            homeTeamScore= getArguments().getInt("homeTeamScore");

            gameLocation= getArguments().getString("gameLocation");
            gameFinished= getArguments().getBoolean("gameFinished");
        }
    }

    private int getTeamLogo(String teamName){
        int logoId = 0;
        switch (teamName){
            case "Bruizers" :
                logoId = R.drawable.bruizers_logo;
                break;
            case "Raiders" :
                logoId = R.drawable.raiders_logo;
                break;
            case "Royals" :
                logoId = R.drawable.royal_logo;
                break;
            case "Sabercats" :
                logoId = R.drawable.sabercat_logo;
                break;
            default:
                break;
        }
        return logoId;
    }
}
