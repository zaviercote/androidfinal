package com.example.zavier.tpfinal.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class League {

    private int id;
    private String nom;
    private List<Team> teams;
    private Scheduel leagueScheduel;
    private List<User> userCollection;

    public League (){this.teams = new ArrayList<>();}
    public League(int id, String nom) {
        this.id = id;
        this.nom = nom;
        this.teams = new ArrayList<>();
        this.leagueScheduel = new Scheduel(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Scheduel getLeagueScheduel() {
        return leagueScheduel;
    }

    public void setLeagueScheduel(Scheduel leagueScheduel) {
        this.leagueScheduel = leagueScheduel;
    }


    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public static League genQJFL(){
        League qjfl = new League(1,"QJFL");
        Team bruizers = new Team(1,"Bruizers");
        Team raiders = new Team(2,"Raiders");
        Team royals = new Team(3,"Royals");
        Team sabercats = new Team(4,"Sabercats");

        qjfl.getTeams().add(bruizers);
        qjfl.getTeams().add(raiders);
        qjfl.getTeams().add(royals);
        qjfl.getTeams().add(sabercats);
        //week1
        Game game1 = new Game(1,"Stade Hebert",14,13);
        Game game2 = new Game(2,"NP Lapierre",23,8);
        game1.setTeams(raiders,royals);
        game2.setTeams(sabercats,bruizers);
        qjfl.getLeagueScheduel().getGames().add(game1);
        qjfl.getLeagueScheduel().getGames().add(game2);
        //week2
        Game game3 = new Game(3,"Cartier Park",9,31);
        Game game4 = new Game(4,"NP Lapierre",32,16);
        game3.setTeams(raiders,sabercats);
        game4.setTeams(royals,bruizers);
        qjfl.getLeagueScheduel().getGames().add(game3);
        qjfl.getLeagueScheduel().getGames().add(game4);
        //week3
        Game game5 = new Game(5,"Cartier Park",14,20);
        Game game6 = new Game(6,"Louis Philipe-Pare",0,43);
        game5.setTeams(royals,sabercats);
        game6.setTeams(bruizers,raiders);
        qjfl.getLeagueScheduel().getGames().add(game5);
        qjfl.getLeagueScheduel().getGames().add(game6);
        //week4
        Game game7 = new Game(7,"NP Lapierre",28,3);
        Game game8 = new Game(8,"Louis Philipe-Pare",14,12);
        game7.setTeams(sabercats,bruizers);
        game8.setTeams(royals,raiders);
        qjfl.getLeagueScheduel().getGames().add(game7);
        qjfl.getLeagueScheduel().getGames().add(game8);
        //week5
        Game game9 = new Game(9,"Stade Hebert",7,23);
        Game game10 = new Game(10,"Cartier Park",0,24);
        game9.setTeams(bruizers,royals);
        game10.setTeams(raiders,sabercats);
        qjfl.getLeagueScheduel().getGames().add(game9);
        qjfl.getLeagueScheduel().getGames().add(game10);
        //week6
        Game game11 = new Game(11,"Stade Hebert",33,6);
        Game game12 = new Game(12,"Louis Philipe-Pare",24,27);
        game11.setTeams(sabercats,royals);
        game12.setTeams(bruizers,raiders);
        qjfl.getLeagueScheduel().getGames().add(game11);
        qjfl.getLeagueScheduel().getGames().add(game12);
        //week7
        Game game13 = new Game(13,"Cartier Park",9,33);
        Game game14 = new Game(14,"Louis Philipe-Pare",17,13);
        game13.setTeams(bruizers,sabercats);
        game14.setTeams(royals,raiders);
        qjfl.getLeagueScheduel().getGames().add(game13);
        qjfl.getLeagueScheduel().getGames().add(game14);
        //week 8
        Game game15 = new Game(15,"NP Lapierre",33,7);
        Game game16 = new Game(16,"Louis Philipe-Pare",38,17);
        game15.setTeams(royals,bruizers);
        game16.setTeams(sabercats,raiders);
        qjfl.getLeagueScheduel().getGames().add(game15);
        qjfl.getLeagueScheduel().getGames().add(game16);
        //week 9
        Game game17 = new Game(17,"Stade Hebert",0,14);
        Game game18 = new Game(18,"NP Lapierre",33,10);
        game17.setTeams(sabercats,royals);
        game18.setTeams(raiders,bruizers);
        qjfl.getLeagueScheduel().getGames().add(game17);
        qjfl.getLeagueScheduel().getGames().add(game18);
        qjfl.finishGames(qjfl.leagueScheduel);
        qjfl.genTeamScheduels();
        return qjfl;
    }

    public void genTeamScheduels(){
        for (Game game:this.getLeagueScheduel().getGames()) {
            String homeTeamName = game.getHomeTeam().getName();
            String awayTeamName = game.getAwayTeam().getName();
            for (Team team:this.getTeams()) {
                if (team.getName().equalsIgnoreCase(homeTeamName)){
                    team.getScheduel().addGame(game);
                }
                else if(team.getName().equalsIgnoreCase(awayTeamName)){
                    team.getScheduel().addGame(game);
                }
            }
        }
    }
    @JsonIgnore
    public String[] getTeamNames(){
        String[] names = new String[this.getTeams().size()];
        for(int i =0;i<this.getTeams().size();i++){
            names[i] = this.getTeams().get(i).getName();
        }
        return names;
    }

    public void finishGames(Scheduel scheduel){
        for (Game game:scheduel.getGames() ) {
            game.setFinished(true);
        }
    }
}
