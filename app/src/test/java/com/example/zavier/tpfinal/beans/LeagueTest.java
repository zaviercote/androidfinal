package com.example.zavier.tpfinal.beans;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LeagueTest {
    League reference;
    League leagueTest;
    @Before
    public void setUp() throws Exception {
        reference = League.genQJFL();
        reference.genTeamScheduels();
    }

    @Test
    public void genQJFL() throws Exception {
        leagueTest = League.genQJFL();
        leagueTest.genTeamScheduels();
        //test un cas au hazard
        assertTrue(leagueTest.getTeams().get(2).getName().equalsIgnoreCase(reference.getTeams().get(2).getName()));
    }

    @Test
    public void genTeamScheduels() throws Exception {
        leagueTest = League.genQJFL();
        leagueTest.genTeamScheduels();
        assertTrue(leagueTest.getTeams().get(1).getScheduel().getGames().size() != 0);
    }

}